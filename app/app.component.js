System.register(['angular2/core', 'angular2/router', './hero/hero.service.ts', './hero/dashboard.component.ts', './hero/heroes.component.ts', './hero/hero-detail.component.ts'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, hero_service_ts_1, dashboard_component_ts_1, heroes_component_ts_1, hero_detail_component_ts_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (hero_service_ts_1_1) {
                hero_service_ts_1 = hero_service_ts_1_1;
            },
            function (dashboard_component_ts_1_1) {
                dashboard_component_ts_1 = dashboard_component_ts_1_1;
            },
            function (heroes_component_ts_1_1) {
                heroes_component_ts_1 = heroes_component_ts_1_1;
            },
            function (hero_detail_component_ts_1_1) {
                hero_detail_component_ts_1 = hero_detail_component_ts_1_1;
            }],
        execute: function() {
            //import { TodoComponent} from './todo/components/todo.component';
            AppComponent = (function () {
                function AppComponent() {
                    this.title = 'Tour of Heroes';
                    this.status = "Show";
                }
                AppComponent.prototype.toggle = function () {
                    this.status = this.status == "Show" ? "active" : "Show";
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        directives: [router_1.ROUTER_DIRECTIVES],
                        templateUrl: './app/main.html',
                        providers: [
                            router_1.ROUTER_PROVIDERS,
                            hero_service_ts_1.HeroService
                        ]
                    }),
                    router_1.RouteConfig([
                        {
                            path: '/',
                            name: 'Dashboard',
                            component: dashboard_component_ts_1.DashboardComponent,
                            useAsDefault: true
                        },
                        {
                            path: '/detail/:id',
                            name: 'HeroDetail',
                            component: hero_detail_component_ts_1.HeroDetailComponent
                        },
                        {
                            path: '/heroes',
                            name: 'Heroes',
                            component: heroes_component_ts_1.HeroesComponent
                        } /*,
                        {
                            path: '',
                            name: 'Todo',
                            component: TodoComponent
                        }*/
                    ]), 
                    __metadata('design:paramtypes', [])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});
//public callData:string;
/*constructor(public http: Http) {
}

getRequest(){
  var authHeader = new Headers();
  authHeader.append('Content-Type', 'text/plain');
  console.log(authHeader);

  this.http.get('http://192.168.0.102:8080/hello?value=hoi', {
    headers: authHeader
  })
  .map(res => res.text())
  .subscribe(
    data => { this.callData = data},
    err => console.error(err),
    () => console.log('done', this.callData)
  );
}
logError(err) {
  console.error('There was an error: ' + err);
}*/
//# sourceMappingURL=app.component.js.map