/**
 * Created by Pieter on 5-4-2016.
 */
import {Component, Output, EventEmitter} from 'angular2/core';

@Component({
    selector:'search-box',
    templateUrl: './app/search/views/search-box.html'
})
export class SearchBox{
    @Output() update = new EventEmitter();

    // on initialized
    ngOnInit(){
        this.update.emit('');
    }
}