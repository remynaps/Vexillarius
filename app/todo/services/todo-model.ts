export class TodoModel{
    constructor(public title:string ="", public status:string = "Started"){}

    toggle():void{
        this.status =
            this.status == "Started"
                ?"Completed"
                :"Started";
    }
}
