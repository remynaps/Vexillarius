import { Injectable } from "angular2/core";
import { TodoModel }  from "./todo-model";

@Injectable()
export class TodoService{
      todos = [
        new TodoModel("Fire-Roasted Herbs & Lamb"),
        new TodoModel("Pressure-Cooked Honey Turkey"),
        new TodoModel("Stir-Fried Apple & Lavender Clams"),
        new TodoModel("Seared Potatoes & Scallops", "Completed"),
        new TodoModel("Infused Cheese & Gratin"),
        new TodoModel("Blanched Pasta & Soup", "Completed"),
        new TodoModel("Cinnamon and Pecan Fudge"),
        new TodoModel("Praline and Coffee Soufflé", "Completed"),
        new TodoModel("Coffee Buns", "Completed"),
        new TodoModel("Grape Genoise")
      ];

        getTodos() {
            return Promise.resolve(this.todos);
        }

        // See the "Take it slow" appendix
        getTodosSlowly() {
            return new Promise<TodoModel[]>(resolve =>
                setTimeout(()=>resolve(this.todos), 2000) // 2 seconds
            );
        }
      addTodo(todo:TodoModel){
          this.todos = [...this.todos, todo];
      }
      toggleTodo(todo:TodoModel)
      {
        const i = this.todos.indexOf(todo); // index of passed todo

        const status = todo.status == "Started" ? "Completed" : "Started";
        const toggledTodo = Object.assign({}, todo, {status});

        this.todos = [
          ...this.todos.slice(0, i),    //... take each todo in the slice and drop it in the array
          toggledTodo,   // add current todo.
          ...this.todos.slice(i+1)  // add all the other todos
        ];
      }
}
