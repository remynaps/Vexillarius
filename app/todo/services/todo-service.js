System.register(["angular2/core", "./todo-model"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, todo_model_1;
    var TodoService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (todo_model_1_1) {
                todo_model_1 = todo_model_1_1;
            }],
        execute: function() {
            TodoService = (function () {
                function TodoService() {
                    this.todos = [
                        new todo_model_1.TodoModel("Fire-Roasted Herbs & Lamb"),
                        new todo_model_1.TodoModel("Pressure-Cooked Honey Turkey"),
                        new todo_model_1.TodoModel("Stir-Fried Apple & Lavender Clams"),
                        new todo_model_1.TodoModel("Seared Potatoes & Scallops", "Completed"),
                        new todo_model_1.TodoModel("Infused Cheese & Gratin"),
                        new todo_model_1.TodoModel("Blanched Pasta & Soup", "Completed"),
                        new todo_model_1.TodoModel("Cinnamon and Pecan Fudge"),
                        new todo_model_1.TodoModel("Praline and Coffee Soufflé", "Completed"),
                        new todo_model_1.TodoModel("Coffee Buns", "Completed"),
                        new todo_model_1.TodoModel("Grape Genoise")
                    ];
                }
                TodoService.prototype.getTodos = function () {
                    return Promise.resolve(this.todos);
                };
                // See the "Take it slow" appendix
                TodoService.prototype.getTodosSlowly = function () {
                    var _this = this;
                    return new Promise(function (resolve) {
                        return setTimeout(function () { return resolve(_this.todos); }, 2000);
                    } // 2 seconds
                     // 2 seconds
                    );
                };
                TodoService.prototype.addTodo = function (todo) {
                    this.todos = this.todos.concat([todo]);
                };
                TodoService.prototype.toggleTodo = function (todo) {
                    var i = this.todos.indexOf(todo); // index of passed todo
                    var status = todo.status == "Started" ? "Completed" : "Started";
                    var toggledTodo = Object.assign({}, todo, { status: status });
                    this.todos = this.todos.slice(0, i).concat([
                        //... take each todo in the slice and drop it in the array
                        toggledTodo
                    ], this.todos.slice(i + 1));
                };
                TodoService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [])
                ], TodoService);
                return TodoService;
            }());
            exports_1("TodoService", TodoService);
        }
    }
});
//# sourceMappingURL=todo-service.js.map