import {Component, Input,Output,EventEmitter} from "angular2/core";

@Component({
    selector: 'todo-item-renderer',
    templateUrl: './app/todo/views/item-render.html'

})
export class TodoItemRenderer{
  @Input() todo;
  @Output() toggle = new EventEmitter();
}
