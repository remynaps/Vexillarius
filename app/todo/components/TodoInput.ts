import{Component} from 'angular2/core';
import{TodoService} from '../services/todo-service';
import{TodoModel} from '../services/todo-model';
@Component({
    selector: 'todo-input',
    templateUrl: './app/todo/views/input.html'
})
export class TodoInput{
  todoModel:TodoModel = new TodoModel();
  constructor(public todoService:TodoService){
    }
  onSubmit()
  {
    this.todoService.addTodo(this.todoModel);
    console.log('value pushed to todoService', this.todoService.todos);
    this.todoModel = new TodoModel();
  }
}
