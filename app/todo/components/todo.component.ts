import { Component, OnInit } from 'angular2/core';

import { TodoModel } from '../services/todo-model';
import { TodoService } from '../services/todo-service';

import {StatusSelector} from './status-selector';
import {SearchBox} from '../../search/components/search-box';
import {TodoList}     from './todo-list';
import {TodoInput}    from './TodoInput';


import {Http,Headers, HTTP_PROVIDERS} from 'angular2/http';

@Component({
    selector: 'my-todo',
    templateUrl: './app/todo/views/',
    directives: [TodoInput, TodoList, SearchBox, StatusSelector],
    viewProviders: [HTTP_PROVIDERS]
})
export class TodoComponent implements OnInit {

    todos: TodoModel[] = [];

    constructor(
        private _todoService: TodoService,
        public http: Http) {
    }

    public callData:string;


    /* getRequest(){
     var authHeader = new Headers();
     authHeader.append('Content-Type', 'text/plain');
     console.log(authHeader);

     this.http.get('http://192.168.0.102:8080/hello?value=hoi', {
        headers: authHeader
     })
     .map(res => res.text())
     .subscribe(
            data => { this.callData = data},
            err => console.error(err),
            () => console.log('done', this.callData)
        );
     }
     logError(err) {
        console.error('There was an error: ' + err);
     }**/




    ngOnInit() {
        this._todoService.getTodos()
            .then(todos => this.todos);
    }
}