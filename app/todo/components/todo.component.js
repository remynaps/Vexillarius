System.register(['angular2/core', '../services/todo-service', './status-selector', '../../search/components/search-box', './todo-list', './TodoInput', 'angular2/http'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, todo_service_1, status_selector_1, search_box_1, todo_list_1, TodoInput_1, http_1;
    var TodoComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (todo_service_1_1) {
                todo_service_1 = todo_service_1_1;
            },
            function (status_selector_1_1) {
                status_selector_1 = status_selector_1_1;
            },
            function (search_box_1_1) {
                search_box_1 = search_box_1_1;
            },
            function (todo_list_1_1) {
                todo_list_1 = todo_list_1_1;
            },
            function (TodoInput_1_1) {
                TodoInput_1 = TodoInput_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            }],
        execute: function() {
            TodoComponent = (function () {
                function TodoComponent(_todoService, http) {
                    this._todoService = _todoService;
                    this.http = http;
                    this.todos = [];
                }
                /* getRequest(){
                 var authHeader = new Headers();
                 authHeader.append('Content-Type', 'text/plain');
                 console.log(authHeader);
            
                 this.http.get('http://192.168.0.102:8080/hello?value=hoi', {
                    headers: authHeader
                 })
                 .map(res => res.text())
                 .subscribe(
                        data => { this.callData = data},
                        err => console.error(err),
                        () => console.log('done', this.callData)
                    );
                 }
                 logError(err) {
                    console.error('There was an error: ' + err);
                 }**/
                TodoComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this._todoService.getTodos()
                        .then(function (todos) { return _this.todos; });
                };
                TodoComponent = __decorate([
                    core_1.Component({
                        selector: 'my-todo',
                        templateUrl: './app/todo/views/',
                        directives: [TodoInput_1.TodoInput, todo_list_1.TodoList, search_box_1.SearchBox, status_selector_1.StatusSelector],
                        viewProviders: [http_1.HTTP_PROVIDERS]
                    }), 
                    __metadata('design:paramtypes', [todo_service_1.TodoService, http_1.Http])
                ], TodoComponent);
                return TodoComponent;
            }());
            exports_1("TodoComponent", TodoComponent);
        }
    }
});
//# sourceMappingURL=todo.component.js.map