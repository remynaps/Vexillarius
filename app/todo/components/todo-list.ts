import {Component, Input} from 'angular2/core';
import {TodoService} from '../services/todo-service';
import {TodoItemRenderer} from './todo-item-renderer';
import {SearchPipe} from '../../search/pipes/search-pipe';
import {StartedPipe} from '../pipes/started-pipe';
@Component({
  selector:'todo-list',
  pipes: [SearchPipe, StartedPipe],
  directives: [TodoItemRenderer],
  templateUrl: 'app/todo/views/list.html'
})
export class TodoList{
  @Input() status;
  @Input() term;
  constructor(public todoService:TodoService){}

}
