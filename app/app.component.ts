﻿import { Component } from 'angular2/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';

import { HeroService } from './hero/hero.service.ts';
import { DashboardComponent } from './hero/dashboard.component.ts';
import { HeroesComponent } from './hero/heroes.component.ts';
import { HeroDetailComponent } from './hero/hero-detail.component.ts';
//import { TodoComponent} from './todo/components/todo.component';


@Component({
    selector: 'my-app',
    directives: [ROUTER_DIRECTIVES  ],//TodoInput, TodoList, SearchBox, StatusSelector],
    templateUrl: './app/main.html',
    providers: [
        ROUTER_PROVIDERS,
        HeroService
    ]
})

@RouteConfig([
    {
        path: '/',
        name: 'Dashboard',
        component: DashboardComponent,
        useAsDefault: true
    },
    {
        path: '/detail/:id',
        name: 'HeroDetail',
        component: HeroDetailComponent
    },
    {
        path: '/heroes',
        name: 'Heroes',
        component: HeroesComponent
    }/*,
    {
        path: '',
        name: 'Todo',
        component: TodoComponent
    }*/
])
export class AppComponent {
    title = 'Tour of Heroes';

    status:string = "Show";

    toggle(){
        this.status = this.status == "Show" ? "active": "Show";
    }
}



//public callData:string;
    /*constructor(public http: Http) {
    }

    getRequest(){
      var authHeader = new Headers();
      authHeader.append('Content-Type', 'text/plain');
      console.log(authHeader);

      this.http.get('http://192.168.0.102:8080/hello?value=hoi', {
        headers: authHeader
      })
      .map(res => res.text())
      .subscribe(
        data => { this.callData = data},
        err => console.error(err),
        () => console.log('done', this.callData)
      );
    }
    logError(err) {
      console.error('There was an error: ' + err);
    }*/

    
    
    
    
    
    
    
    

